import { Fragment } from 'react'
import Link from 'next/link'

const MovieListItem = ({ movie }) => (
    <Fragment>
        <Link as={movie.imdbID} href={`/movie?imdbID=${movie.imdbID}`}>
            <img src={movie.Poster}/>
        </Link>
        <style jsx>{`
        img {
            max-width: 300px;
        }
        `}</style>
    </Fragment>
)

export default MovieListItem