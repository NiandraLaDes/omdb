import { Fragment } from 'react'
import helpers from '../helpers/utils'

const SearchBar = ({ title }) => (
    <Fragment>
        <input type="search" name="s" onChange={helpers.changePage} placeholder="Search movies..." value={title} />
        <style jsx>{`
        input {
            margin: 3em 0;
            padding:.8em;
            width:100%;
            max-width:600px;
        }
        `}</style>
    </Fragment>
)

export default SearchBar