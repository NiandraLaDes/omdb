import { Fragment } from 'react'
import Head from './Head'
import SearchBar from './SearchBar'

const Layout = (props) => (
    <Fragment>
        <Head />
        <div id="wrapper">
            <main>{props.children}</main>
            <footer>Made by <a href="https://enter-webdevelopment.nl" target="_blank">Enter Webdevelopment</a></footer>
        </div>
        <style jsx global>{`
        * {
            box-sizing: border-box;
            padding:0;
            margin:0
        }
        html,
        body,
        body > div {
            width: 100%;
            height: 100%;
        }
        body {
            padding:0 1em;
        }
        #wrapper {
            min-height: 100%;
            display: flex;
            flex-direction: column;
            align-items: stretch;
        }
        main {
            max-width: 1300px;
            margin: 0 auto 20px;
            min-height: 100%;
            flex-grow: 1;
        }
        footer{
            padding: 1.5em 0;
            text-align:center;
            border-top: 1px solid #ddd;
        }
        main, footer {
            flex-shrink: 0;
        }

        @media screen and (min-width:768px) {
            body {
                padding: 0.5em 2em 0;
            }
        }
        `}</style>
    </Fragment>
)

export default Layout
