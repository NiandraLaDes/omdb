import { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import helpers from '../helpers/utils'

const LEFT_PAGE = ' < '
const RIGHT_PAGE = ' > '

/**
 * Helper method for creating a range of numbers
 * range(1, 5) => [1, 2, 3, 4, 5]
 */
const range = (from, to , step = 1) => {
    let i = from
    const range = []

    while (i <= to) {
        range.push(i)
        i += step
    }
    return range
}

class Pagination extends Component {
    constructor(props) {
        super(props)
        const { totalResults = 0, pageLimit = 20, currentPage =  1, pageNeighbours = 2 } = props

        this.totalResults = parseInt(totalResults)
        this.pageLimit = parseInt(pageLimit)
        this.pageNeighbours = parseInt(pageNeighbours)

        this.totalPages = Math.ceil(totalResults / this.pageLimit)
        this.state = {currentPage}
    }

    goToPage = (page) => {
        this.setState({currentPage: page})
        helpers.changeQueryParams(null, page);
    }

    fetchPageNumbers = () => {
        const pageNeighbours = this.pageNeighbours
        const totalPages = this.totalPages
        const currentPage = parseInt(this.props.currentPage)

        /**
         * totalNumbers: the total page numbers to show on the control
         * totalBlocks: totalNumbers + 2 to cover for the left(<) and right(>) controls
         */
        const totalNumbers = (pageNeighbours * 2) + 3
        const totalBlocks = totalNumbers + 2

        if (totalPages > totalBlocks ) {
            const startPage = Math.max(2, currentPage - pageNeighbours)
            const endPage = Math.min(totalPages - 1, currentPage + pageNeighbours)

            let pages = range(startPage, endPage)

            /**
             * hasLeftSpill: has hidden pages to the left
             * hasRightPages: has hidden pages to the right
             * pagesOffset: number of hidden pages either to the left or to the right
             */
            const hasLeftSpill = startPage - 2;
            const hasRightSpill = (totalPages - endPage) > 1
            const spillOffset = totalNumbers - (pages.length + 1);

            switch (true) {
                // handle: (1) < {5 6} [7] {8 9} (10)
                case (hasLeftSpill && !hasRightSpill): {
                    const extraPages = range(startPage - spillOffset, startPage - 1);
                    pages = [LEFT_PAGE, ...extraPages, ...pages];
                    break;
                }

                // handle: (1) {2 3} [4] {5 6} > (10)
                case (!hasLeftSpill && hasRightSpill): {
                    const extraPages = range(endPage + 1, endPage + spillOffset);
                    pages = [...pages, ...extraPages, RIGHT_PAGE];
                    break;
                }

                // handle: (1) < {4 5} [6] {7 8} > (10)
                case (hasLeftSpill && hasRightSpill):
                default: {
                    pages = [LEFT_PAGE, ...pages, RIGHT_PAGE];
                    break;
                }
            }
            return [1, ...pages, totalPages];
        }
        return range(1, totalPages);
    }

    render() {
        if (!this.totalResults || this.totalPages === 1) return null

        const { currentPage } = this.state
        const pages = this.fetchPageNumbers();

        return (
            <Fragment>
                <nav>
                    <ul>
                        {pages.map((page, index) => {
                            if (page === LEFT_PAGE) {
                                return <li key={index} onClick={() => this.goToPage(currentPage - 1)}>Previous</li>
                            };

                            if (page === RIGHT_PAGE) {
                                return <li key={index} onClick={() => this.goToPage(currentPage + 1)}>Next</li>
                            };

                            return (
                                <li key={index} className={currentPage === page ? ' active' : ''}  onClick={() => this.goToPage(page)}>{ page }</li>
                            );
                        })}
                    </ul>
                </nav>
                <style jsx>{`
                ul {
                    display:flex;
                    list-style-type: none;
                    flex-wrap: wrap
                }
                li {
                    border: 1px solid #ddd;
                    cursor: pointer;
                    margin-right:0.3em;
                    margin-bottom:0.3em;
                    padding:1em;
                }
                .active {
                    color: white;
                    background-color: blue;
                }
                `}</style>
            </Fragment>
        );

    }
}

Pagination.propTypes = {
    totalResults: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
    ]),
    pageLimit: PropTypes.number,
    currentPage: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
    ]),
    pageNeighbours: PropTypes.number
}

export default Pagination