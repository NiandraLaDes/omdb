import MovieListItem from './MovieListItem'

const MovieList = ({ movies }) => (
    <ul>
        {movies.map((movie) =>
            <li key={movie.imdbID}>
                <MovieListItem movie={movie} />
            </li>
        )}
        <style jsx>{`
        ul {
            display:flex;
            flex-wrap: wrap;
            list-style-type: none;
            margin:0;
            padding: 0;
            justify-content: flex-start;
        }
        li {
            flex:1;
            padding:0.2em;
            cursor: pointer;
        }
        `}</style>
    </ul>
)

export default MovieList