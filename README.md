# Project Title

Search App for Open Movie Database

# Description

A server-side rendered React app using [Next.js](https://github.com/zeit/next.js/) to search for movies
using the [Omdbapi](http://www.omdbapi.com/)

## Built With

* [React.js](https://reactjs.org/) - A JavaScript library for building user interfaces.
* [Next.js](https://nextjs.org/) - A lightweight framework for static and server-rendered applications.

## Authors

* **Bart van Enter** - *Initial work* - [Bitbucket](https://bitbucket.org/NiandraLaDes) and [Enter Webdevelopment](https://enter-webdevelopment.nl/)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details