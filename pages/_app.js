import React from 'react'
import App, { Container } from 'next/app'
import Layout from '../components/Layout'

class MyApp extends App {
    static async getInitialProps({ Component, ctx }) {
        let pageProps = {}

        // get data and pass them down as props to pages
        if(Component.getInitialProps) {
            pageProps = await Component.getInitialProps(ctx)
        }
        return { pageProps }
    }

    render() {
        const {Component, pageProps} = this.props
        return (
            <Container>
                <Layout>
                    <Component {...pageProps} />
                </Layout>
            </Container>
        )
    }
}

export default MyApp