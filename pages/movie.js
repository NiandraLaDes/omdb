import React, { Component } from 'react'
import fetch from 'isomorphic-unfetch'
import Config from '../config'

class Movie extends Component {
    static async getInitialProps(context) {
        //get id of movie
        const { imdbID } = context.query

        //get movies from api
        const res = await fetch(`${Config.apiUrl}&i=${imdbID}&plot=full`)
        let data = await res.json();
        console.dir(data)
        return { data }
    }

    render() {
        const { data } = this.props
        return (
            <article>
                <header>
                    <h1>{data.Title} <span>({data.Year})</span></h1>
                    <p>{data.Runtime} | {data.Genre} | {data.Released}</p>
                </header>
                <img src={data.Poster} />
                <div>
                    <section>
                        <p>{data.Plot}</p>
                        <dl>
                            <div>
                                <dt>Director</dt>
                                <dd>{data.Director}</dd>
                            </div>
                            <div>
                                <dt>Writer</dt>
                                <dd>{data.Writer}</dd>
                            </div>
                            <div>
                                <dt>Actors</dt>
                                <dd>{data.Actors}</dd>
                            </div>
                        </dl>
                    </section>
                    {data.Ratings &&
                        <section>
                            <h2>Ratings</h2>
                            <dl>
                            {data.Ratings.map(rating =>
                                <div key={rating.Source}>
                                    <dt>{rating.Source}</dt>
                                    <dd>{rating.Value}</dd>
                                </div>
                            )}
                            </dl>
                        </section>
                    }
                    {data.Awards &&
                        <section>
                            <h2>Awards</h2>
                            {data.Awards}
                        </section>
                    }
                </div>
                <style jsx>{`
                h1 {
                    font-size: 1.8em;
                }
                h2 {
                    font-size: 1.5em;
                }
                img {
                    width:100%;
                }
                dl > div {
                    display: flex;
                    flex-wrap: wrap;
                }
                dt {
                    color: #666666;
                    font-weight: bold;
                    margin-right:.3em
                }
                dt:after {
                    content: ":"
                }
                section p {
                    margin: 1em 0;
                }
                @media screen and (min-width: 768px) {
                    h1 {
                        font-size: 2em;
                    }
                    article {
                        display:grid;
                        grid-template-columns: 1fr 2fr;
                        grid-gap: 2em;
                        margin: 0 auto;
                        max-width: 850px;
                    }
                    header {
                        grid-column: 1 / -1;
                    }
                    section {
                        margin-bottom:2em;
                    }
                    section p {
                        margin: 0 0 1em;
                    }
                }
                `}</style>
            </article>
        )
    }
}

export default Movie