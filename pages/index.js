import React, {Component, Fragment} from 'react'
import helpers from '../helpers/utils'
import SearchBar from '../components/SearchBar'
import MovieList from '../components/MovieList'
import Pagination from "react-js-pagination";

class Index extends Component {
    static async getInitialProps(context) {
        const { query } = context
        let data = {}
        if (!query.s) return { data, query }

        /********
         * Build 2 request urls for 2 pages ( 10 result per page )
         * witch the query params and fetch results
         */
        const requests = helpers.getRequests(query)
        data = await Promise
            .all(requests)
            .then((results) => {
                /*****
                 * Results are at least the response of first request (True or False)
                 * If second request is True, then merge the Search results of first and second request
                 */
                let retVal = {...results[0]}
                if( results[0].Search && results[1].Search ) {
                    retVal.Search = [
                        ...results[0].Search,
                        ...results[1].Search
                    ]
                }
                return retVal
            }).catch(() => {{Error: "Something went wrong"}})

        return { data, query }
    }

    render() {
        let { data, query } = this.props
        return (
            <article>
                <SearchBar />
                {data.Error && data.Error}
                {data.Search && <MovieList movies={data.Search} />}
                {data.totalResults > 20 &&
                    <Pagination
                        activePage={query.p}
                        itemsCountPerPage={20}
                        totalItemsCount={data.totalResults}
                        pageRangeDisplayed={5}
                        onChange={helpers.changePage}
                    />
                }
                <style jsx>{`
                article {
                    text-align:center
                }
                `}</style>
            </article>
        )
    }
}

export default Index
