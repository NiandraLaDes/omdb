import Router from 'next/router'
import Config from '../config'

const helpers = {
    changePage(param) {
        /**
         * Push new url with pagenumber or input fields
         */
        const newValues = {}
        console.dir(param);
        if (typeof param === "object") {
            newValues[param.target.name] = param.target.value
            newValues.p = 1
        } else {
            newValues.p = param
        }

        const queryParams = Object.assign(Router.query, newValues)
        Router.push({
            pathname: '/',
            query: {
                ...queryParams
            }
        })
        return false
    },
    getRequests(query) {
        const pages = this.getPages(query.p)
        const queryString = this.getQueryString(query)

        return pages.map(async page => {
            const res = await fetch(`${Config.apiUrl}&${queryString}&page=${page}`)
            return res.json()
        })
    },
    getPages(page = 1) {
        /**
         * Return Two pages based on the given page parameter
         * First page => Last page - 1
         * Last page => Page param * 2
         */
        const lastPage = parseInt(page) * 2
        const firstPage = lastPage - 1
        return [firstPage, lastPage]
    },
    getQueryString(query) {
        // transform query Object into a query string
        return Object.keys(query)
            .map(key => `${key}=${query[key]}`)
            .join('&')
    },
    orderBy(data, orderBy) {
        if (orderBy === 'Year') {
            data.sort((a, b) => {
                var aYear = a.Year.substring(0, 4);
                var bYear = b.Year.substring(0, 4);
                if (aYear < bYear) return -1;
                if (aYear > bYear) return 1;
                return 0;
            })
        } else {
            data.sort((a, b) => {
                var aTitle = a.Title.toUpperCase()
                var bTitle = b.Title.toUpperCase()
                if (aTitle < bTitle) return -1;
                if (aTitle > bTitle) return 1;
                return 0;
            })
        }
        return data
    }
}

export default helpers